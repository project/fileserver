
REQUIREMENTS
------------
This module is designed for Drupal 6.x, and requires File Framework[1] and
dav.module[2]. (Additional PHP version and PEAR library requirements are
detailed in the documentation for dav.module.)

Last tested with fileframework-6.x-1.x of 2008-Aug-28 and dav-6.x-1.0-alpha2.

[1] http://drupal.org/project/fileframework
[2] http://drupal.org/project/dav

INSTALLATION
------------
1. Copy all the module files into a subdirectory called modules/fileserver/
   under your Drupal installation directory.

2. Go to administer >> modules and enable the `dav' and `fileserver'
   modules.

3. Go to administer >> settings >> dav to review and change the DAV
   configuration options to your liking (e.g. enable the DAV server).

4. Go to administer >> settings >> dav >> fileserver to review and change
   the configuration options to your liking.

5. Connect to http://www.yoursite.com/dav/ with your DAV client. Use your
   Drupal user name and password to login.
