
DESCRIPTION
-----------
Provides WebDAV access to Drupal's taxonomies and files.

FEATURES
--------
* Unified authentication and access controls: just use your Drupal user name
  and password to login to DAV, too.
* Exports taxonomy vocabularies as top-level DAV collections, and
  taxonomy terms (aka categories) as DAV sub-collections (aka folders).
* Exports file nodes as DAV resources. Currently supports only the 'file'
  content type provided by file.module.
* Allows drag-and-drop upload of files, with automatic file node creation
  and association with the taxonomy term the file was dropped onto.
* Allows in-place renames or deletions of folders (taxonomy terms).
* Allows in-place modifications to existing files.
* Allows copying/moving folders and files both within the mounted DAV
  hierarchy as well as to/from the local file system.
* The web display of a DAV collection index is entirely themeable.
* Integrates with the OG Vocabulary module, if installed, to restrict access
  to only vocabularies in those groups the user is subscribed to, as well as
  to associate files with the proper group when they are associated with
  taxonomy terms belonging to that group.

INSTALLATION
------------
Please refer to the accompanying file INSTALL.txt for installation
requirements and instructions.

LIMITATIONS
-----------
* Taxonomy-based DAV collections have the current date as their creation and
  last modification date, since no other date information is stored for
  taxonomy terms in Drupal.
* Files can't be copied or moved into the root collection or the first-level
  collections, since nodes can't be associated directly with vocabularies.

CREDITS
-------
Originally developed by Arto Bendiken <http://bendiken.net/>
Ported to Drupal 6.x by Justin R. Miller <http://codesorcery.net/>
Sponsored by MakaluMedia Group <http://www.makalumedia.com/>
Sponsored by M.C. Dean, Inc. <http://www.mcdean.com/>
Sponsored by SPAWAR <http://www.spawar.navy.mil/>
PEAR HTTP_WebDAV_Server developed by Hartmut Holzgraefe <hartmut@php.net>
